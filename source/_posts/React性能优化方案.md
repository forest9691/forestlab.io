---
title: React性能优化方案
date: 2023-11-29 12:36:54
tags: react
---


##### 思考：分离React组件稳定的部分与变化的部分

是对父组件操作的.

变化的部分包含state、props、context。将变化的部分抽离出去形成一个独立的组件



##### 为什么要进行React组件的性能优化

- 为什么React需要性能优化API
- React性能优化应该遵循的法则
- 性能优化背后的源码进行机制



##### 举例1

在Input框只输入内容，会在`input content`中显示出来，如果子组件是一个比较枆时的组件，那么`input content`显示时会有卡顿现象。

![image-20231129112234121](images/react_profiler.png)

```jsx
import React, { useState } from 'react';
export default function App() {
    const [text, setInput] = useState("")
    const style = {
        padding: "20px",
        border: "1px solid blue"
    }
    return (<div style={{width: 300, margin: 50, padding: 10, border: "1px solid #ccc"}}>
        <div style={style}>
            <input onChange={(e) => setInput(e.target.value)} />
            <div></div>
            input content:  {text}
        </div>
        <Child></Child>
    </div>)
}

function Child() {

    const now = performance.now();
    while (performance.now() - now < 300) { }

    console.log("Child Component visiable")
    const style = {
        margin: "20px 0 0",
        border: "1px solid #ccc",
        padding: 5
    }
    return (
        <div style={style}>
            这是一个子组件模块
        </div>
    );

}
```

原因：

`input`子组件的改变会在父组件`<App/>`中更新state,并且会触发父组件中子组件的更新

解决办法1：

将`input`相关的部分提取出来形成单独的新组件，相关的部分也就是变化的部分，提取后`<App/>`组件就是稳定的部分了。新组件的状态改变只会对新组件内部进行更新。也就是说变化的部分局部化到一个新组件。

```jsx
import React, { useState } from 'react';
export default function App() {
    return (<div style={{ width: 300, margin: 50, padding: 10, border: "1px solid #ccc" }}>
        <Input />
        <Child></Child>
    </div>)
}

function Input() {

    const [text, setInput] = useState("")
    const style = {
        padding: "20px",
        border: "1px solid blue"
    }
    return (
        <div style={style}>
            <input onChange={(e) => setInput(e.target.value)} />
            <div></div>
            input content:  {text}
        </div>
    )
}

function Child() {

    const now = performance.now();
    while (performance.now() - now < 300) { }

    console.log("Child Component visiable")
    const style = {
        margin: "20px 0 0",
        border: "1px solid #ccc",
        padding: 5
    }
    return (
        <div style={style}>
            这是一个子组件模块
        </div>
    );

}
```

解决办法2：

使用`React.memo()`方法来记忆props有没有改变来判断要不要更新子组件。

```jsx
import React, { useState } from 'react';

const Child = React.memo(function Child() {

    const now = performance.now();
    while (performance.now() - now < 300) { }

    console.log("Child Component visiable")
    const style = {
        margin: "20px 0 0",
        border: "1px solid #ccc",
        padding: 5
    }
    return (
        <div style={style}>
            这是一个子组件模块
        </div>
    );

})

export default function App() {

    const [text, setInput] = useState("")
    const style = {
        padding: "20px",
        border: "1px solid blue"
    }
    return (
        <div style={{ width: 300, margin: 50, padding: 10, border: "1px solid #ccc" }}>
            <div style={style}>
                <input onChange={(e) => setInput(e.target.value)} />
                <div></div>
                input content:  {text}
            </div>
            <Child></Child>
        </div>
    )
}
```

##### 举例2

这个与`举例1`相比有一点点区别，就是在外面`div`中定义了一个`title`属性。在这种情况下也会造成页面有卡顿的现象。

![image-20231129112234121](images/react_profiler.png)

```jsx
import React, { useState } from 'react';

export default function App() {

    const [text, setInput] = useState("")
    const style = {
        padding: "20px",
        border: "1px solid blue"
    }
    return (<div title={text} style={{ width: 300, margin: 50, padding: 10, border: "1px solid #ccc" }}>
        <div style={style}>
            <input onChange={(e) => setInput(e.target.value)} />
            <div></div>
            input content:  {text}
        </div>
        <Child />
    </div>
    )
}

function Child() {

    const now = performance.now();
    while (performance.now() - now < 300) { }

    console.log("Child Component visiable")
    const style = {
        margin: "20px 0 0",
        border: "1px solid #ccc",
        padding: 5
    }
    return (
        <div style={style}>
            这是一个子组件模块
        </div>
    );

}
```

原因：

与`举例1`相同

解决办法：

就是把将父组件抽取出来形成一个单独的新组件，把子组件`<Child/>`作为新组件的属性`children`传递进去。

```jsx
import React, { useState } from 'react';

export default function App() {

    return (
        <>
            <InputWrapper>
        		<!-- Child组件不会更新，原因是父组件的更新没有给子组件传递prop，对于子组件来说state、prop都没有改变它是不会更新的 -->	
                <!-- 如果在InputWrapper组件同的{children}改为 {React.cloneElement(children)}后，input每次修改后，Child组件都会更新 -->
                <Child></Child>
            </InputWrapper>
        </>
    )
}

function InputWrapper({ children }) {
    const [text, setInput] = useState("")
    const style = {
        padding: "20px",
        border: "1px solid blue"
    }
    return (<div title={text} style={{ width: 300, margin: 50, padding: 10, border: "1px solid #ccc" }}>
        <div style={style}>
            <input onChange={(e) => setInput(e.target.value)} />
            <div></div>
            input content:  {text}
        </div>
        {children}
    </div>
    )
}

function Child() {
    const now = performance.now();
    while (performance.now() - now < 300) { }

    console.log("Child Component visiable")
    const style = {
        margin: "20px 0 0",
        border: "1px solid #ccc",
        padding: 5
    }
    return (
        <div style={style}>
            这是一个子组件模块
        </div>
    );

}
```




##### 性能优化方案

- 抽取变化的部分

- 抽象变化并组合组件

- 函数组件用`React.memo()`

- 类组件使用`PureComponent`，进行浅比较

- `useEffect(function, [dependencies])`

- 类组件中`shouldComponentUpdate(nextProps, nextState)`可以进行身比较

- `const Home = React.lazy(() => import(/*webpackChunkName:"Home"*/"../App.jsx"))`

  - 注释为了更改加载文件名
  - `<Suspense/>`需要与`React.lazy()`一起使用

- `<Fragment>` 提升渲染

- 避免使用内联样式属性

  - 建议使用css文件

- 避免使用内联函数。

  - `<input value={this.state.value} onChange={(e)=>this.setState({value: e.target.value})}`

- 在构造函数中进行函数this绑定。

- 在类组件中使用箭头函数

- 优化条件渲染心提升组件性能

  - `true && <Component/>`

- 为组件创建错误边界

  - 定义`componentDidCatch` 、 `static getDerivedStateFromError`

  - 如果你定义了 `componentDidCatch`，那么 React 将在某些子组件（包括后代组件）在渲染过程中抛出错误时调用它。这使得你可以在生产中将该错误记录到错误报告服务中。

    一般来说，它与 [`static getDerivedStateFromError`](https://zh-hans.react.dev/reference/react/Component#static-getderivedstatefromerror) 一起使用，这样做允许你更新 state 来响应错误并向用户显示错误消息。具有这些方法的组件称为 **错误边界**。



