---
title: 简易Modal工作原理
date: 2023-12-22 11:51:55
tags: react

---

只写了最简单的逻辑，包括打开和关闭，并没有添加样式，只是想表达弹框的工作原理

```jsx
import { Button, Flex, Modal, ConfigProvider } from 'antd'
// import { render, unmount } from 'rc-util/lib/React/render';
import { render as reactRender, unmount as reactUnmount } from 'rc-util/lib/React/render';
import React, { useRef, useLayoutEffect } from "react";
import { createPortal } from 'react-dom';

const container = document.createDocumentFragment();

const InternalButton = React.forwardRef(function InternalButton(a, ref) {
    // console.log(a)
    return (
        <>
            <Button
                ref={ref}
                onClick={(e) => {
                    showEffect()
                }}
            >
                打开
            </Button>
            <Button
                ref={ref}
                onClick={(e) => {
                    reactUnmount(container)
                }}
            >
                关闭
            </Button>
        </>

    )
});

export default function App() {
    const ref = useRef(0);
    return (
        <Flex>
            {/* <ConfigProvider wave={{ showEffect }}>
                <InternalButton ref={ref}></InternalButton>
            </ConfigProvider> */}
            <InternalButton ></InternalButton>
        </Flex>
    )
}


const showEffect = function (target, info) {
    reactRender(<ShowWaveEffect></ShowWaveEffect>, container);
}

function ShowWaveEffect({ target }) {
    const devRef = useRef(null);
    return (
        <>
            <Portal>
                <p onClick={() => {
                    console.log("children")
                }}>这个子节点被放置在 document body 中。</p>
            </Portal>
            <div ref={devRef}>
                show wave
            </div>
        </>
    )
}

function Portal({ children }) {

    const div = document.createElement("div")

    function cleanup() {
        div.parentElement?.removeChild(div);
    }

    useLayoutEffect(() => {
        document.body.appendChild(div);
        return cleanup;
    });

    return (
        createPortal(
            children,
            div,
            "forest9691"
        )
    )
}

```
