---
title: Chrome浏览器DevTools的快捷键
date: 2023-11-30 09:34:37
tags: Chrome
---


| dev面板功能                           | Window       | Mac          |
| :------------------------------------- | ------------ | ------------ |
| search全站关键字搜索                  | Ctrl+Shift+F | option+cmd+F |
| `run command`搜索框                   | Ctrl+Shift+P | shift+cmd+P  |
| 打开查找文件搜索框                    | Ctrl+F       | cmd+P        |
| 显示或隐藏`console`标签               | Esc          | Esc          |
| 在文件中搜索                          |              | cmd+F        |
| 打开`console`标签                     | Ctrl+`       | Ctrl+`       |
| 清除`clonsole history`和`network`内容 | Ctrl+L       | Ctrl+L       |
| 复现`Element`结点上的style            | Ctrl+Alt+C   | option+cmd+C |
| 重复加载`devTools`                    | Alt+R        | option+R     |
| 打开`settings`面板                    | F1           | F1           |
| 隐藏`settings`标签                    | Esc          | Esc          |
|                                       |              |              |
|                                       |              |              |
|                                       |              |              |
|                                       |              |              |
