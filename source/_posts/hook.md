---
layout: p
title: hook
date: 2023-11-27 18:06:28
tags: hook
---



useEffect 是一个 React Hook，它允许你 将组件与外部系统同步。

`useDeferredValue` 是一个 React Hook，可以让你延迟更新 UI 的某些部分。

`useImperativeHandle` 是 React 中的一个 Hook，它能让你自定义由 [ref](https://react.docschina.org/learn/manipulating-the-dom-with-refs) 暴露出来的句柄。

`memo` 允许你的组件在 props 没有改变的情况下跳过重新渲染。

`useInsertionEffect` 可以在布局副作用触发之前将元素插入到 DOM 中。

`useLayoutEffect` 是 [`useEffect`](https://react.docschina.org/reference/react/useEffect) 的一个版本，在浏览器重新绘制屏幕之前触发。

在组件的顶层作用域调用 `useReducer` 以创建一个用于管理状态的 [reducer](https://react.docschina.org/learn/extracting-state-logic-into-a-reducer)。

`useRef` 是一个 React Hook，它能帮助引用一个不需要渲染的值。

`useState` 是一个 React Hook，它允许你向组件添加一个 [状态变量](https://react.docschina.org/learn/state-a-components-memory)。

`useSyncExternalStore` 是一个让你订阅外部 store 的 React Hook。

`useTransition` 是一个帮助你在不阻塞 UI 的情况下更新状态的 React Hook。

`useCallback` 是一个允许你在多次渲染中缓存函数的 React Hook。

`useContext` 是一个 React Hook，可以让你读取和订阅组件中的 [context](https://react.docschina.org/learn/passing-data-deeply-with-context)。

`useDebugValue` 是一个 React Hook，可以让你在 [React 开发工具](https://react.docschina.org/learn/react-developer-tools) 中为自定义 Hook 添加标签。
