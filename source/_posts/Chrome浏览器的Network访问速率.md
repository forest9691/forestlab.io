---
title: 修改Network访问速率
date: 2023-11-30 11:42:17
tags: Chrome
---
![image_2023-11-30-12-18-34](images/image_2023-11-30-12-18-34.png)

# 第一种:Presets方式

- fast 3G
- slow 3G
- offline

# 第二种：Custom方式

- 打开`devTools`面板
- 按`F1`打开`Settings`面板
- 点击左侧菜单的`Throttling`选项
- 点击`Add custom profile`按钮
- 添加`ProfileName`、下载速率、上传速率、`Latency延迟时间`
- 按`Esc`
- 配置完后，点击`Network`标签
- 在`Disable cache`后面有一个下拉框，找到`Custom`下的自定义的选项。
- 刷新页面就能实现配置请求速率，可以调试在低速率的情况下应用表现情况
