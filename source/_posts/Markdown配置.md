---
title: Markdown配置
date: 2023-11-30 09:58:43
tags: markdown
---


# 声明区块
> 第一层
> > 第二层嵌套
> > > 第三层嵌套

# 代码块
```javascript
$(document).ready(function () {
    alert('Hello Markdown');
});
```
# 链接
```Markdown
[链接名称](链接地址)
或者
<链接地址>
```


# 高级链接
```Markdown
这个链接用 1 作为网址变量 [Google][1]
这个链接用 Jayhrn 作为网址变量 [Jayhrn][Jayhrn]
然后在文档的结尾为变量赋值（网址）

[1]: http://www.google.com/
[Jayhrn]: https://blog.jayhrn.com/
```
这个链接用 1 作为网址变量 [Google][1]
这个链接用 Jayhrn 作为网址变量 [Jayhrn][Jayhrn]
然后在文档的结尾为变量赋值（网址）

[1]: http://www.google.com/
[Jayhrn]: https://blog.jayhrn.com/

# 图片
```Markdown
![alt 属性文本](图片地址)

![alt 属性文本](图片地址 "可选标题")

<img src="https://img.jayhrn.com/img/posts/20220727/003.webp" width="25%">
```

# 表格
```Markdown
| 左对齐 | 右对齐 | 居中对齐 |
| :-----| ----: | :----: |
| 单元格 | 单元格 | 单元格 |
| 单元格 | 单元格 | 单元格 |
```
| 左对齐 | 右对齐 | 居中对齐 |
| :-----| ----: | :----: |
| 单元格 | 单元格 | 单元格 |
| 单元格 | 单元格 | 单元格 |

# 高级Markdown技巧

## html元素
```Markdown
使用 <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Del</kbd> 重启电脑
```
使用 <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Del</kbd> 重启电脑



