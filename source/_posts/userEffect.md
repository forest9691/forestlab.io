---
title: userEffect
date: 2023-11-25 16:02:23
tags: react
---

`useEffect` 是一个 React Hook，它允许你 [将组件与外部系统同步](https://react.docschina.org/learn/synchronizing-with-effects)。

> 有些组件需要与网络、某些浏览器 API 或第三方库保持连接，当它们显示在页面上时。这些系统不受 React 控制，所以称为外部系统。
>
> useEffect相当于是函数组件与外部系统进行通信交流的一个API接口，组件内部与外部系统进行交互都是通过useEffect来完成的

```javascript
useEffect(setup, dependencies?/* 依赖项列表 */)
// 当组件被添加到 DOM 的时候，React 将运行 setup 函数。
```

#### 注意事项 

- `useEffect` 是一个 Hook，因此只能在 **组件的顶层** 或自己的 Hook 中调用它，而不能在循环或者条件内部调用。如果需要，抽离出一个新组件并将 state 移入其中。
- 如果你 **没有打算与某个外部系统同步**，[那么你可能不需要 Effect](https://react.docschina.org/learn/you-might-not-need-an-effect)。
- 当严格模式启动时，React 将在真正的 setup 函数首次运行前，**运行一个开发模式下专有的额外 setup + cleanup 周期**。这是一个压力测试，用于确保 cleanup 逻辑“映射”到了 setup 逻辑，并停止或撤消 setup 函数正在做的任何事情。如果这会导致一些问题，[请实现 cleanup 函数](https://react.docschina.org/learn/synchronizing-with-effects#how-to-handle-the-effect-firing-twice-in-development)。
- 如果你的一些依赖项是组件内部定义的对象或函数，则存在这样的风险，即它们将 **导致 Effect 过多地重新运行**。要解决这个问题，请删除不必要的 [对象](https://react.docschina.org/reference/react/useEffect#removing-unnecessary-object-dependencies) 和 [函数](https://react.docschina.org/reference/react/useEffect#removing-unnecessary-function-dependencies) 依赖项。你还可以 [抽离状态更新](https://react.docschina.org/reference/react/useEffect#updating-state-based-on-previous-state-from-an-effect) 和 [非响应式的逻辑](https://react.docschina.org/reference/react/useEffect#reading-the-latest-props-and-state-from-an-effect) 到 Effect 之外。
- 如果你的 Effect 不是由交互（比如点击）引起的，那么 React 会让浏览器 **在运行 Effect 前先绘制出更新后的屏幕**。如果你的 Effect 正在做一些视觉相关的事情（例如，定位一个 tooltip），并且有显著的延迟（例如，它会闪烁），那么将 `useEffect` 替换为 [`useLayoutEffect`](https://react.docschina.org/reference/react/useLayoutEffect)。
- 即使你的 Effect 是由一个交互（比如点击）引起的，**浏览器也可能在处理 Effect 内部的状态更新之前重新绘制屏幕**。通常，这就是你想要的。但是，如果你一定要阻止浏览器重新绘制屏幕，则需要用 [`useLayoutEffect`](https://react.docschina.org/reference/react/useLayoutEffect) 替换 `useEffect`。
- Effect **只在客户端上运行**，在服务端渲染中不会运行。



# useEffect适用

- 函数组件内部与外部系统进行交流的接口
  - 当组件的props或state改变后，就会调用useEffect()
  - 外部系统状态改变时通过定义在useEffect()中的监听来改变函数组件的状态或props
    - window.addEventLiestener("resize", fun)
    - localStorage

- 每个 Effect 表示一个独立的同步过程。 
  - 一个useEffect()应该只做一件事，useEffect中的逻辑应该是共同重用的。
- 请求数据
- 指定响应式依赖项 
  - 响应式值包括 props 和直接在组件内声明的所有变量和函数
    - 在 Effect 中读取响应式值时，必须将其添加为依赖项。
      - 原因：这样可以确保你的 Effect 对该值的每次更改都“作出响应”
    - `React Hook useEffect has a missing dependency: 'roomId'. Either include it or remove the dependency array.`
  - 依赖项为空数组的 Effect 不会在组件任何的 props 或 state 发生改变时重新运行
- 在 Effect 中根据先前 state 更新 state
- 删除不必要的对象依赖项
  - 避免使用渲染期间创建的对象作为依赖项。相反，在 Effect 内部创建对象
- 删除不必要的函数依赖项
  - 避免使用在渲染期间创建的函数作为依赖项，请在 Effect 内部声明它
- 从 Effect 读取最新的 props 和 state
  - 有时你想要从 Effect 中获取 最新的 props 和 state，而不“响应”它们。
    - `useEffectEvent`使用它




