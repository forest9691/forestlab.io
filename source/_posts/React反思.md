---
title: React反思
date: 2023-12-07 10:04:07
tags: react
---

# 抽象能力＝编程能力

编程能力就是抽象能力，其中最重要的一个项就是命名能力。

```jsx
export default function App() {
    const style = { 
        width: 300, 
        margin: 50, 
    };
    // 这里的命令好否的标准就是抽象能力
    const searchStyle = {
        padding: "20px",
        border: "1px solid blue"
    }
    const search = (
    	<div style={searchStyle}>
            <input onChange={(e) => setInput(e.target.value)} />
            <div></div>
            input content:  {text}
        </div>
    )
    const classString = classNames(
        prefixCls,
        {
          [`${prefixCls}-loading`]: loading,
        },
        className,
    );
    return (
        <!--style的属性值提取出来命名能力就是抽象能力-->
        <div className={classString}>
            {search}
            <Child></Child>
        </div>
    )
}
```

## React 哲学

首先，会把它分解成一个个 **组件**

> 函数组件、类组件、ＤＯＭ组件

然后，你需要把这些组件连接在一起

> 嵌套组件、渲染属性、高阶组件

最后，使数据流经它们

> state、props、context


