---
title: 关于我
date: 2023-11-25 16:02:23
---

Forest9691是一名软件设计师(已领证)，目前从事餐饮行业SAAS软件WEB方面的开发工作，带领过3-5人的团队。最早从事后端Java方面的开发，后来转型为WEB方面的开发。WEB方面目前使用的技术栈主要有React、Redux、Router、TS，使用AntD的UI组件。

从事基于Ubuntu平台WEB开发工作，日常使用的技术包含HTML、LESS、JS、React、Redux、Router、TS等。　　

熟悉常用设计模式、计算机网络、数据结构与算法、数据库、程序设计等。

开发工具VSCODE、VIM等。
