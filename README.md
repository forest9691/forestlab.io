# Hexo博客　　
Ｈexo博客官网：[https://hexo.io/zh-cn](https://hexo.io/zh-cn)　　

# Hexo主题
[https://github.com/jerryc127/hexo-theme-butterfly](https://github.com/jerryc127/hexo-theme-butterfly)

# Ｈexo部署
[https://hexo.io/zh-cn/docs/one-command-deployment](https://hexo.io/zh-cn/docs/one-command-deployment)

# Hexo Code主题
https://github.com/highlightjs/highlight.js/blob/main/src/styles/github-dark.css

# 创建一篇新文章
你可以执行下列命令来创建一篇新文章或者新的页面。
```bash
$ hexo new [layout] <title>
```
您可以在命令中指定文章的布局（layout），默认为 post，可以通过修改 _config.yml 中的 default_layout 参数来指定默认布局。